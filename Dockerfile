# obsrv-httpd
FROM debian:jessie-slim

LABEL maintainer="gkelle"

ENV DEBIAN_FRONTEND=noninteractive \
    HOME=/opt/app-root

ARG APACHE_MIRROR=http://apache.mirrors.pair.com
ARG APACHE_VERSION=2.4.43
ARG APACHE_APR_VERSION=1.7.0
ARG APACHE_APR_UTIL_VERSION=1.6.1
ARG OPENSSL_VERSION=1.1.1f
ARG PHP_VERSION="5"
ARG PHP_PACKAGE="php${PHP_VERSION} php${PHP_VERSION}-fpm php${PHP_VERSION}-mysql"
ARG PHP_FPM_LISTEN_PORT="9000"
ENV PHP_FPM_BIN="/usr/sbin/php${PHP_VERSION}-fpm"

ARG APACHE_DOWNLOAD_URL=${APACHE_MIRROR}/httpd/httpd-${APACHE_VERSION}.tar.gz
ARG APACHE_APR_DOWNLOAD_URL=${APACHE_MIRROR}/apr/apr-${APACHE_APR_VERSION}.tar.gz
ARG APACHE_APR_UTIL_DOWNLOAD_URL=${APACHE_MIRROR}/apr/apr-util-${APACHE_APR_UTIL_VERSION}.tar.gz
ARG OPENSSL_DOWNLOAD_URL=https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz
ARG APACHE_PREFIX="${HOME}"
ARG OPENSSL_PREFIX="/opt/openssl-${OPENSSL_VERSION}"
ARG BUILD_PACKAGES="curl ca-certificates perl make gcc libpcre3 libpcre3-dev libexpat1 libexpat1-dev libxml2 libxml2-dev libxslt1-dev libxslt1.1"
ARG PACKAGES="${PHP_PACKAGE} dnsutils"
ARG HTTPD_CONF_FILES="${HOME}/src/etc/apache2/sites-available/*.conf"

RUN apt-get update \
    && useradd -u 1001 -r -g 0 -d ${HOME} -s /sbin/nologin  -c "Default Application User" default \
    && apt-get install -y --no-install-recommends ${BUILD_PACKAGES} ${PACKAGES} \
    && curl -s ${OPENSSL_DOWNLOAD_URL} -o /tmp/openssl-${OPENSSL_VERSION}.tar.gz \
    && curl -s ${APACHE_DOWNLOAD_URL} -o /tmp/apache-${APACHE_VERSION}.tar.gz \
    && curl -s ${APACHE_APR_DOWNLOAD_URL} -o /tmp/apache-apr-${APACHE_APR_VERSION}.tar.gz \
    && curl -s ${APACHE_APR_UTIL_DOWNLOAD_URL} -o /tmp/apache-apr-util-${APACHE_APR_UTIL_VERSION}.tar.gz \
    && mkdir /tmp/build \
    && cd /tmp/build \
    && tar zxf /tmp/openssl-${OPENSSL_VERSION}.tar.gz \
    && cd openssl-${OPENSSL_VERSION} \
    && ./config --prefix=${OPENSSL_PREFIX} --openssldir=/etc/ssl shared enable-weak-ssl-ciphers enable-ssl3 enable-ssl3-method enable-ssl2 -Wl,-rpath=${OPENSSL_PREFIX}/lib \
    && make \
    && make install \
    && echo "${OPENSSL_PREFIX}/lib" > /etc/ld.so.conf.d/ps2.conf \
    && cd /tmp/build \
    && tar zxvf /tmp/apache-${APACHE_VERSION}.tar.gz \
    && cd httpd-${APACHE_VERSION} \
    && tar zxf /tmp/apache-apr-${APACHE_APR_VERSION}.tar.gz -C srclib/ \
    && tar zxf /tmp/apache-apr-util-${APACHE_APR_UTIL_VERSION}.tar.gz -C srclib/ \
    && ln -sf apr-${APACHE_APR_VERSION} srclib/apr \
    && ln -sf apr-util-${APACHE_APR_UTIL_VERSION} srclib/apr-util \
    && ./configure --prefix=${APACHE_PREFIX} --with-included-apr --with-ssl=${OPENSSL_PREFIX} --enable-ssl \
    && make \
    && make install \
    && /bin/sed -i -e '/^export LD_LIBRARY_PATH/i LD_LIBRARY_PATH="'"${OPENSSL_PREFIX}"'/lib:\$LD_LIBRARY_PATH"' -e '$a export APACHE_LOG_DIR='"${HOME}"'/logs' ${APACHE_PREFIX}/bin/envvars \
    && /bin/sed -i -e 's/^Listen 80/Listen 8080/' -e '$a Include '"${HTTPD_CONF_FILES}"'' -e 's:^#LoadModule proxy_module modules/mod_proxy.so:LoadModule proxy_module modules/mod_proxy.so:' -e 's:^#LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so:LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so:' -e 's:^#LoadModule ssl_module modules/mod_ssl.so:LoadModule ssl_module modules/mod_ssl.so:' -e 's:^#LoadModule rewrite_module modules/mod_rewrite.so:LoadModule rewrite_module modules/mod_rewrite.so:' -e 's/^User daemon/User default/' -e 's/^Group daemon/Group root/' ${APACHE_PREFIX}/conf/httpd.conf \
    && /bin/sed -i -E -e 's/^(user|listen.owner) = (.*)/;\1 = \2/' -e 's/^(group|listen.group) = (.*)/;\1 = \2/'  -e "s:^listen = .*:listen = ${PHP_FPM_LISTEN_PORT}:" -e 's/;clear_env = no/clear_env = no/' /etc/php5/fpm/pool.d/www.conf \
    && /bin/sed -i -e 's:^error_log = .*:error_log = '"${APACHE_PREFIX}"'/logs/php5-fpm.log:' -e 's:^pid = .*:pid = '"${HOME}"'/php5-fpm.pid:' /etc/php5/fpm/php-fpm.conf \
    && cd / \
    && apt-get remove -y curl make gcc libexpat1-dev libpcre3-dev libxslt1-dev \
    && rm -rf /tmp/build \
    && rm /tmp/*.tar.gz \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p ${HOME}/src \
    && chown -R 1001:0 ${HOME} ${APACHE_PREFIX} ${OPENSSL_PREFIX} \
    && chmod -R g+rwx ${HOME} ${APACHE_PREFIX}

ENV BUILDER_VERSION 1.4

LABEL io.k8s.description="Builder for obsrv httpd" \
      io.k8s.display-name="obsrv-httpd builder" \
      io.openshift.expose-services="8080:http,8443:https" \
      io.openshift.tags="builder,obsrv" \
      io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

COPY ./s2i/bin/ /usr/libexec/s2i

USER 1001

EXPOSE 8080
EXPOSE 8443

WORKDIR ${HOME}/src

CMD ["/usr/libexec/s2i/usage"]
